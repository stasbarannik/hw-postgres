CREATE TABLE IF NOT EXISTS cars  (name varchar(100), price float, model varchar(50), year int);

CREATE TABLE company (name varchar(50) PRIMARY KEY unique, country varchar(100), year_est int);

INSERT INTO company (name, country, year_est) values ('hyundai', 'South Korea', 1967),  ('Toyota', 'Japan', 1937);

select model, max(price)
from cars right join company c on cars.model =  c.name 
WHERE year > 2015
GROUP by model
HAVING max(price) >= 20000;

select * from company;

select * from cars;


/******* Homework *******/

CREATE OR REPLACE FUNCTION price_calculating() RETURNS TRIGGER AS $$
	BEGIN
		NEW.price_uah = NEW.price_usd * 27;
		NEW.price_eur = NEW.price_usd * 0.85;
		RETURN NEW;
	END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS price_calculating ON cars;

CREATE TRIGGER price_calculating BEFORE INSERT OR UPDATE ON cars FOR EACH ROW EXECUTE PROCEDURE price_calculating()

/******* Homework end *******/


DELETE from cars;

INSERT INTO cars (name, price_usd, model, year) values ('i30', 15000, 'hyundai', 2019),
('i20', 12000, 'hyundai', 2018),
('Tucson', 20000, 'hyundai', 2013),
('Accent', 18000, 'hyundai', 2020),
('Camry', 24000, 'Toyota', 2017),
('RAV4', 22000, 'Toyota', 2015),
('Land Cruiser', 30000, 'Toyota', 2014); 

select * from cars;
